//
//      HCRL Axon: TMS320F28335
//      (c) Nicholas Paine
//      (c) Frank Bormann
//
//
//###########################################################################
//
// FILE:	Axon_main.c
// 
// TITLE:	HCRL Axon control code for eRockhead joint controller
//
//###########################################################################
#include "DSP2833x_Device.h"

// external function prototypes
extern void InitAdc(void);
extern void InitSysCtrl(void);
extern void InitPieCtrl(void);
extern void InitPieVectTable(void);
extern void InitCpuTimers(void);
extern void ConfigCpuTimer(struct CPUTIMER_VARS *, float, float);
extern void display_ADC(unsigned int);

// Prototype statements for functions found within this file.
void Gpio_select(void);
interrupt void cpu_timer0_isr(void);
interrupt void adc_isr(void);		 // ADC  End of Sequence ISR

// Global Variables
unsigned int Voltage_VR1;
unsigned int Voltage_VR2;

//###########################################################################
//						main code									
//###########################################################################
void main(void)
{
	InitSysCtrl();	// Basic Core Init from DSP2833x_SysCtrl.c

	EALLOW;
   	SysCtrlRegs.WDCR= 0x00AF;	// Re-enable the watchdog 
   	EDIS;			// 0x00AF  to NOT disable the Watchdog, Prescaler = 64

	DINT;				// Disable all interrupts
	
	Gpio_select();		// GPIO9, GPIO11, GPIO34 and GPIO49 as output
					    // to 4 LEDs at Peripheral Explorer)

	InitPieCtrl();		// basic setup of PIE table; from DSP2833x_PieCtrl.c
	
	InitPieVectTable();	// default ISR's in PIE

	InitAdc();			// Basic ADC setup, incl. calibration

	AdcRegs.ADCTRL1.all = 0;	   
	AdcRegs.ADCTRL1.bit.ACQ_PS = 7; 	// 7 = 8 x ADCCLK	
	AdcRegs.ADCTRL1.bit.SEQ_CASC =1; 	// 1=cascaded sequencer
	AdcRegs.ADCTRL1.bit.CPS = 0;		// divide by 1
	AdcRegs.ADCTRL1.bit.CONT_RUN = 0;	// single run mode

	AdcRegs.ADCTRL2.all = 0;			
	AdcRegs.ADCTRL2.bit.INT_ENA_SEQ1 = 1;	// 1=enable SEQ1 interrupt
	//AdcRegs.ADCTRL2.bit.EPWM_SOCA_SEQ1 =1;	// 1=SEQ1 start from ePWM_SOCA trigger
	AdcRegs.ADCTRL2.bit.INT_MOD_SEQ1 = 0;	// 0= interrupt after every end of sequence


	AdcRegs.ADCTRL3.bit.ADCCLKPS = 3;	// ADC clock: FCLK = HSPCLK / 2 * ADCCLKPS
										// HSPCLK = 75MHz (see DSP2833x_SysCtrl.c)
										// FCLK = 12.5 MHz

	AdcRegs.ADCMAXCONV.all = 0x0001;    // 2 conversions from Sequencer 1

	AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0; // Setup ADCINA0 as 1st SEQ1 conv.
    AdcRegs.ADCCHSELSEQ1.bit.CONV01 = 1; // Setup ADCINA1 as 2nd SEQ1 conv.

	EALLOW;
	PieVectTable.TINT0 = &cpu_timer0_isr;
	PieVectTable.ADCINT = &adc_isr;
	EDIS;

	InitCpuTimers();	// basic setup CPU Timer0, 1 and 2

	ConfigCpuTimer(&CpuTimer0,150,1000); //timer at 1ms

	PieCtrlRegs.PIEIER1.bit.INTx7 = 1;		// CPU Timer 0
	PieCtrlRegs.PIEIER1.bit.INTx6 = 1;		// ADC

	IER |=1;

	EINT;
	ERTM;

	CpuTimer0Regs.TCR.bit.TSS = 0;	// start timer0

	while(1)
	{    
		AdcRegs.ADCTRL2.bit.SOC_SEQ1 = 1;	// 1= Start of conversion trigger for SEQ1
		GpioDataRegs.GPBSET.bit.GPIO61 = 1; //start ADC converstion
	  	while(CpuTimer0.InterruptCount <1) // wait for 1 ms
	  	{
	  		// wait for 1 ms
			EALLOW;
			SysCtrlRegs.WDKEY = 0x55;		// Service watchdog #1
			EDIS;
		}

		CpuTimer0.InterruptCount = 0;
	}
} 

void Gpio_select(void)
{
	EALLOW;
	GpioCtrlRegs.GPAMUX1.all = 0;		// GPIO15 ... GPIO0 = General Puropse I/O
	GpioCtrlRegs.GPAMUX2.all = 0;		// GPIO31 ... GPIO16 = General Purpose I/O
	GpioCtrlRegs.GPBMUX1.all = 0;		// GPIO47 ... GPIO32 = General Purpose I/O
	GpioCtrlRegs.GPBMUX2.all = 0;		// GPIO63 ... GPIO48 = General Purpose I/O
	GpioCtrlRegs.GPCMUX1.all = 0;		// GPIO79 ... GPIO64 = General Purpose I/O
	GpioCtrlRegs.GPCMUX2.all = 0;		// GPIO87 ... GPIO80 = General Purpose I/O
	 
	GpioCtrlRegs.GPADIR.all = 0;
	GpioCtrlRegs.GPADIR.bit.GPIO9 = 1;	// peripheral explorer: LED LD1 at GPIO9
	GpioCtrlRegs.GPADIR.bit.GPIO11 = 1;	// peripheral explorer: LED LD2 at GPIO11

	GpioCtrlRegs.GPBDIR.all = 0;		// GPIO63-32 as inputs
	GpioCtrlRegs.GPBDIR.bit.GPIO34 = 1;	// peripheral explorer: LED LD3 at GPIO34
	GpioCtrlRegs.GPBDIR.bit.GPIO49 = 1; // peripheral explorer: LED LD4 at GPIO49
	GpioCtrlRegs.GPBDIR.bit.GPIO60 = 1;	// peripheral explorer: header
	GpioCtrlRegs.GPBDIR.bit.GPIO61 = 1;	// peripheral explorer: header

	GpioCtrlRegs.GPCDIR.all = 0;		// GPIO87-64 as inputs
	EDIS;
}   

interrupt void cpu_timer0_isr(void)
{
	CpuTimer0.InterruptCount++;
	GpioDataRegs.GPBTOGGLE.bit.GPIO60 = 1;
	EALLOW;
	SysCtrlRegs.WDKEY = 0xAA;	// service WD #2
	EDIS;
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

interrupt void  adc_isr(void)
{
	GpioDataRegs.GPBCLEAR.bit.GPIO61 = 1; //end ADC converstion
	Voltage_VR1 = AdcMirror.ADCRESULT0;	// store results global
  	Voltage_VR2 = AdcMirror.ADCRESULT1;
	// Reinitialize for next ADC sequence
  	AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;       // Reset SEQ1
  	AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;		// Clear INT SEQ1 bit
  	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1; // Acknowledge interrupt to PIE
}

//===========================================================================
// End of SourceCode.
//===========================================================================
